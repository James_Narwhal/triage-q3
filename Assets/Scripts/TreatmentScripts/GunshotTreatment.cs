﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunshotTreatment : MonoBehaviour
{
    private bool cleaned;
    private bool hand;
    private bool bandaged;
    private GameObject thigh;
    private float applyTimer;

    public bool handOver = false;
    public bool alcOver = false;
    public bool bandageOver = false;
    public bool switches = false;


    public GameObject H1;
    public GameObject change;
    public GameObject current;
    public GameObject global;
    public GameObject Loadbar;
    public GameObject eButton;

    public Transform applyBar;

    public AudioSource tape;
    public AudioSource scream;
    public AudioSource open;
    public AudioSource AmbientPain;
    private bool playingOpen = false;
    private bool applyProcess = false;
    // Start is called before the first frame update
    void Start()
    {
        bandaged = false;
        hand = false;
        cleaned = false;
        H1 = GameObject.FindGameObjectWithTag("HealthBar");
        AmbientPain.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (handOver == true && hand == false)
        {
            if (applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    applyTimer = 1.0f;
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer, 1f);
                if (applyTimer <= 0)
                {
                    Debug.Log("CompleteHand");
                    hand = true;
                    Loadbar.SetActive(false);
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e"))
            {
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if (handOver == false && hand == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        if (cleaned == false && alcOver == true && ResourceCount.alcCount > 0 && hand == true)
        {
            if(applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e") )
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 2.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                if (playingOpen == false)
                {
                    open.Play();
                    playingOpen = true;
                }
                applyBar.localScale = new Vector3(applyTimer / 2.0f, 1f);
                if (applyTimer <= 0)
                {
                    scream.Play();
                    ResourceCount.alcCount -= 1;
                    Debug.Log("CompleteAlc");
                    cleaned = true;
                    Loadbar.SetActive(false);
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e"))
            {
                open.Stop();
                Loadbar.SetActive(false);
                playingOpen = false;
                applyProcess = false;
            }
        }
        else if (alcOver == false && hand == true && cleaned == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }

        
        if  (bandaged == false && bandageOver == true && cleaned == true && hand == true && ResourceCount.bandageCount > 0)
        {
            if (applyProcess == false)
            {
                eButton.GetComponent<SpriteRenderer>().enabled = true;
            }
            if (Input.GetKey("e"))
            {
                if (applyProcess == false)
                {
                    eButton.GetComponent<SpriteRenderer>().enabled = false;
                    applyTimer = 4.0f;
                    applyProcess = true;
                }
                Loadbar.SetActive(true);
                applyTimer -= Time.deltaTime;
                applyBar.localScale = new Vector3(applyTimer / 4.0f, 1f);
                if (applyTimer <= 0)
                {
                    tape.Play();
                    ResourceCount.bandageCount -= 1;
                    Debug.Log("Bandages Left:" + ResourceCount.bandageCount);
                    AmbientPain.Stop();
                    current.SetActive(false);
                    change.SetActive(true);
                    Loadbar.SetActive(false);
                    bandaged = true;
                    applyProcess = false;
                }
            }
            else if (Input.GetKeyUp("e"))
            {
                Loadbar.SetActive(false);
                applyProcess = false;
            }
        }
        else if(bandageOver == false && hand == true && cleaned == true && bandaged == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
        }



        if (bandaged == true && hand == true && cleaned == true && switches == false)
        {
            eButton.GetComponent<SpriteRenderer>().enabled = false;
            Debug.Log("CompleteP1");
            H1.GetComponent<HealthBar>().done = true;
            H1.GetComponent<HealthBar>().treated = true;
            switches = true;
            //set Treated == true;
            //set Done == true;
        }
    }

}
