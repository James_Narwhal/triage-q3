﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBandage : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Gunshot;
    public GameObject Cut;
    public GameObject scrapes;

    public string scrapesString;
    void Start()
    {
        Gunshot = GameObject.FindGameObjectWithTag("Gunshot(leg)");
        Cut = GameObject.FindGameObjectWithTag("Cut");
        scrapesString = scrapes.gameObject.tag;
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag != "Gunshot(leg)" && col.gameObject.tag != "Cut" && col.gameObject.tag != scrapesString)
        {
            Physics2D.IgnoreCollision(Gunshot.GetComponent<Collider2D>(), Gunshot.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(Cut.GetComponent<Collider2D>(), Cut.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(scrapes.GetComponent<Collider2D>(), scrapes.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "Gunshot(leg)")
        {
            Debug.Log("Collision");
            Gunshot.GetComponent<GunshotTreatment>().bandageOver = true;
        }
        if (col.gameObject.tag == "Cut")
        {
            Cut.GetComponent<LeftAmputationTreatment>().bandageOver = true;
        }
        if (col.gameObject.tag == scrapesString)
        {
            if (scrapesString == "HeadHurt")
            {
                scrapes.GetComponent<Concussion>().bandageOver = true;
            }
            else
            {
                scrapes.GetComponent<Scrapes>().bandageOver = true;
            }

        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag != "Gunshot(leg)" && col.gameObject.tag != "Cut")
        {
            Physics2D.IgnoreCollision(Gunshot.GetComponent<Collider2D>(), Gunshot.GetComponent<Collider2D>());
            Physics2D.IgnoreCollision(Cut.GetComponent<Collider2D>(), Cut.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == "Gunshot(leg)")
        {
            Debug.Log("Collision");
            Gunshot.GetComponent<GunshotTreatment>().bandageOver = false;
        }
        if (col.gameObject.tag == "Cut")
        {
            Cut.GetComponent<LeftAmputationTreatment>().bandageOver = false;
        }

        if (col.gameObject.tag == scrapesString)
        {
            if (scrapesString == "HeadHurt")
            {
                scrapes.GetComponent<Concussion>().bandageOver = false;
            }
            else
            {
                scrapes.GetComponent<Scrapes>().bandageOver = false;
            }

        }
    }
}
