﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSplint : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject Splint;
    private string splintString;
    void Start()
    {
        splintString = Splint.gameObject.tag;
        
        //Splint = GameObject.FindGameObjectWithTag("SnappedLeg");
    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.gameObject.tag != splintString)
        {
            Physics2D.IgnoreCollision(Splint.GetComponent<Collider2D>(), Splint.GetComponent<Collider2D>());

        }
        if (col.gameObject.tag == splintString)
        {
            if (splintString == "SnappedLeg")
            {
                Debug.Log("Collision");
                Splint.GetComponent<LegSplint>().splintOver = true;
            }
            else
            {
                Splint.GetComponent<Splint>().splintOver = true;
            }

        }
        if(col.gameObject.tag == "SnappedLeg")
        {
            Debug.Log("Collision");
            Splint.GetComponent<LegSplint>().splintOver = true;
        }
    }
    void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.tag != splintString)
        {
            Physics2D.IgnoreCollision(Splint.GetComponent<Collider2D>(), Splint.GetComponent<Collider2D>());
        }
        if (col.gameObject.tag == splintString)
        {
            if (splintString == "SnappedLeg")
            {
                Debug.Log("Collision");
                Splint.GetComponent<LegSplint>().splintOver = false;
            }
            else
            {
                Splint.GetComponent<Splint>().splintOver = false;
            }

        }

    }
}
