﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PretreatmentPage : MonoBehaviour
{
    public int briefingNum; // Which briefing is this?
    public int tutorialPageNum; // How many tutorial pages are there in total?
    public int currentTutorialPage; // Which tutorial page is shown right now?
    public Sprite[] tutorialPages; // Array of sprites of the tutorial pages.

    // Start is called before the first frame update
    public void Start()
    {
        tutorialPages = new Sprite[tutorialPageNum + 1]; // Initialize the array.
        for (int x = 1; x < tutorialPageNum + 1; x++)
        {
            string num = x.ToString(); // Make the variable a string.
            // Find the path to the correct sprite.
            string myPath = "Briefing" + briefingNum + "Sprites/Page" + num;

            // Make a sprite variable and initialize it with that path.
            Sprite toAdd = Resources.Load<Sprite>(myPath);

            // Add it to the array.
            tutorialPages[x] = toAdd;
        }
        // By default, the first tutorial page is shown.
        this.GetComponent<SpriteRenderer>().sprite = tutorialPages[currentTutorialPage];
    }
}