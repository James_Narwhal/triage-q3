﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PretreatmentMove : MonoBehaviour
{
    private PretreatmentPage myPage; // Make a PretreatmentPage object to use.
    public AudioSource turn;
    // Start is called before the first frame update
    public void Start()
    {
        myPage = GameObject.FindObjectOfType<PretreatmentPage>(); // Find the page that already is in the scene.
    }

    public void OnButtonPress()
    {
        turn.Play();
        if (this.name == "RightArrow") // If the right arrow is clicked on
        {
            
            if (myPage.currentTutorialPage != myPage.tutorialPageNum) // If the player isn't on the last page
            {
                myPage.currentTutorialPage += 1; // Go to the next page and change the sprite accordingly.
                myPage.GetComponent<SpriteRenderer>().sprite = myPage.tutorialPages[myPage.currentTutorialPage];
            }
        }
        else if (this.name == "LeftArrow") // Same thing as above.
        {
            if (myPage.currentTutorialPage != 1) // If the player isn't on the first page
            {
                myPage.currentTutorialPage -= 1; // Go to the previous page and change the sprite accordingly.
                myPage.GetComponent<SpriteRenderer>().sprite = myPage.tutorialPages[myPage.currentTutorialPage];
            }
        }
    }
}
