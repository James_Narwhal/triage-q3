﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class BandageCount : MonoBehaviour
{
    public Transform target;
    public Text bandageCount;
    public Text applyResources;
    public int bandaged;
    public float bandageTime = 5.0f;


    public GameObject bandages;
    public int currentCount;
    public bool removeBandage = false;
    public bool isOn = false;


    public float defaultPositionX;
    public float defaultPositionY;

    public GameObject HP;
    public GameObject HP2;

    private void Start()
    {
        bandages = GameObject.FindGameObjectWithTag("bandage");
        defaultPositionX = -5.37f;
        defaultPositionY = -3.39f;
        bandaged = 0;
        HP = GameObject.FindGameObjectWithTag("HealthBar");
        HP2 = GameObject.FindGameObjectWithTag("health2");
        currentCount = 5;
    }


    // Update is called once per frame
    void Update()
    {

        //x bounds for head = 0.9 - 2.0
        //x bounds for body = 2.5 - 4.3


        bandageCount.text = "x" + currentCount;
        if (isOn == true)
        {
            if (Input.GetKey("e") && currentCount != 0 && bandageTime > 0)
            {
                bandageTime -= Time.deltaTime;

                applyResources.text = "Applying Bandages\n" + bandageTime.ToString("F2");
                if (bandageTime <= 0)
                {
                    applyResources.text = "";
                    HP.GetComponent<HealthBar>().newHealth += .2f;
                    bandaged++;
                    removeBandage = true;
                }
            }
        }
        if (isOn == false)
        {
            bandageTime = 5.0f;
        }
        /*if (bandages.transform.localPosition.x > 2.5 && bandages.transform.localPosition.x < 4.3)
        {


            if (Input.GetKey("e") && currentCount != 0 && bandageTime > 0)
            {

                bandageTime -= 0.01f;

                applyResources.text = "Applying Bandages\n" + bandageTime.ToString("F2");

                //apply the bandages
                // if (bandageTime > 0)
                //  {



                // }
                if (bandageTime <= 0)
                {
                    
                    applyResources.text = "";
                    bandaged++;
                    HP.GetComponent<HealthBar>().newHealth += .2f;
                    removeBandage = true;
                }
                


            }
        }*/
        if (removeBandage == true)
        {
            applyResources.text = "";
            currentCount -= 1;
            removeBandage = false; //simple flag checker to decrement the resource by 1
            bandageTime = 5.0f; //set the timer back to default

            //snap that shit back to the starting position

            bandages.transform.localPosition = new Vector3(defaultPositionX, defaultPositionY, 0);


        }

        if (currentCount == 0) //when you run out delete the associated object
        {
            bandages.transform.gameObject.SetActive(false);
        }
        //checks if the player has bandaged all parts of the patient
        if (bandaged >= 3)
        {
            //HP.GetComponent<HealthBar>().Coun = 0f;
            Invoke("switchGameOver", 2f);
        }
    }
    void switchGameOver()
    {
    }
}
