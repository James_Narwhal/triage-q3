﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class resourceCheck : MonoBehaviour
{
    public GameObject current;
    public Text textTarget;

    private string textName;
    // Start is called before the first frame update
    void Start()
    {
        textName = textTarget.gameObject.name;
    }

    // Update is called once per frame
    void Update()
    {
        if (textName == "waterNum")
        {
            if(ResourceCount.waterCount <= 0 )
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if(ResourceCount.waterCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
        if (textName == "splintNum")
        {
            if (ResourceCount.splintCount <= 0)
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if (ResourceCount.splintCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
        if (textName == "bandageNum")
        {
            if (ResourceCount.bandageCount <= 0)
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if (ResourceCount.bandageCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
        if (textName == "alcNum")
        {
            if (ResourceCount.alcCount <= 0)
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if (ResourceCount.alcCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
        if (textName == "burngelNum")
        {
            if (ResourceCount.burngelCount <= 0)
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if (ResourceCount.burngelCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
        if (textName == "threadNum")
        {
            if (ResourceCount.threadCount <= 0)
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if (ResourceCount.threadCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
        if (textName == "morphineNum")
        {
            if (ResourceCount.morphineCount <= 0)
            {
                current.GetComponent<ClickDragResource>().enabled = false;
            }
            else if (ResourceCount.morphineCount > 0)
            {
                current.GetComponent<ClickDragResource>().enabled = true;
            }
        }
    }
}
