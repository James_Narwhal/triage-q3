﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenu : MonoBehaviour
{
    private TMP_InputField inputFieldName;
    public GameObject playerInfo;
    public void PlayGame()
    {
        inputFieldName = transform.Find("InputField").GetComponent<TMP_InputField>();
        PlayerInfo.PlayerName = inputFieldName.ToString();
        SceneManager.LoadScene("Briefing1");
    }

    public void Exit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}
